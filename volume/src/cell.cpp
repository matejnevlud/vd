#include "stdafx.h"

#include "cell.h"

Cell::Cell( const DATA_TYPE * rhos, const Vector3 & A, const Vector3 & B )
{
	memcpy( rhos_, rhos, sizeof( rhos_ ) );

	bounds_ = AABB( A, B );
}

Cell::~Cell()
{

}

float Cell::Gamma( const Vector3 & uvw ) const
{
	//return ( rho_A() + rho_B() + rho_C() + rho_D() + rho_E() + rho_F() + rho_G() + rho_H() ) / 8.0f;

	const float & u_t = uvw.x;
	const float & v_t = uvw.y;
	const float & w_t = uvw.z;

	// trilinear interpolation
	const COMP_TYPE alpha_AB = rho_A() * ( 1 - u_t ) + rho_B() * u_t;
	const COMP_TYPE alpha_DC = rho_D() * ( 1 - u_t ) + rho_C() * u_t;
	const COMP_TYPE alpha_EF = rho_E() * ( 1 - u_t ) + rho_F() * u_t;
	const COMP_TYPE alpha_HG = rho_H() * ( 1 - u_t ) + rho_G() * u_t;

	const COMP_TYPE beta_0 = alpha_AB * ( 1 - v_t ) + alpha_DC * v_t;
	const COMP_TYPE beta_1 = alpha_EF * ( 1 - v_t ) + alpha_HG * v_t;

	return static_cast< DATA_TYPE >( beta_0 * ( 1 - w_t ) + beta_1 * w_t );

	// TODO try to add tricubic interpolation
}


int getMaskValue(int x, int y, int z) {
    int mask[3][3] = {{1,2,1}, {2,4,2}, {1,2,1}};
    switch (z) {
        case 0:
            return 0;
        case -1:
            return mask[x+1][y+1];
        case 1:
            return -mask[x+1][y+1];
    }
}

float Cell::computeSobelinDir(const Vector3 & uvw, Vector3 dir, float d = 0.2f) const {
    float sum = 0;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            for (int dz = -1; dz <= 1; dz++) {
                Vector3 pos = uvw + Vector3(dx*d, dy*d, dz*d);
                if (dir.x == 1) {
                    sum += this->Gamma(this->u(pos)) * getMaskValue(dy, dz, dx);
                } else if (dir.y ==1) {
                    sum += this->Gamma(this->u(pos)) * getMaskValue(dx, dz, dy);
                } else {
                    sum += this->Gamma(this->u(pos)) * getMaskValue(dx, dy, dz);
                }
            }
        }
    }
    return sum;
}

Vector3 Cell::GradGamma( const Vector3 & uvw ) const
{
	// TODO compute the gradient of the scalar field here (use finite central differences or Sobel�Feldman operator)

    /*
    float xDiff = data[x-1, y, z] - data[x+1, y, z;]
    float yDiff = data[x, y-1, z] - data[x, y+1, z;]
    float zDiff = data[x, y, z-1] - data[x, y, z+1;]
    float3 gradient = float3(xDiff, yDirr, zDiff)
    float3 normal = normalize(gradient);
    */
    float e = 0.9;
    float xDiff = Gamma(u(uvw - Vector3(1, 0, 0) * e)) - Gamma(u(uvw + Vector3(1, 0, 0) * e));
    float yDiff = Gamma(u(uvw - Vector3(0, 1, 0) * e)) - Gamma(u(uvw + Vector3(0, 1, 0) * e));
    float zDiff = Gamma(u(uvw - Vector3(0, 0, 1) * e)) - Gamma(u(uvw + Vector3(0, 0, 1)));

    Vector3 gradient = Vector3(xDiff, yDiff, zDiff);
    gradient.Normalize();
    return gradient;

    return Vector3(
            this->computeSobelinDir(uvw, Vector3(1, 0, 0)),
            this->computeSobelinDir(uvw, Vector3(0, 1, 0)),
            this->computeSobelinDir(uvw, Vector3(0, 0, 1))
    );
}

float Cell::Integrate( Ray & ray, const float t0, const float t1 ) const
{
	// TODO approximate computation of an integral of scalar values along the given segment of the ray
	Vector3 sum;
	float dx = 0.1f;
	for ( float t = t0; t <= t1; t += dx ) {
		//sample_color.x = sample_color.y = sample_color.z = MAX( sample_color.x, actual_cell.Gamma( actual_cell.u( ray.eval( t ) ) ) );
		float q = Gamma( u( ray.eval( t ) ) ) * dx;
		sum += Vector3( q, q, q );
	}

	return sum.x;
}

float Cell::FindIsoSurface( Ray & ray, const float t0, const float t1, const float iso_value ) const
{
	// TODO find the parametric distance of the iso surface of the certain iso value along the given segment of the ray
    float dx = 0.1f;
    for ( float t = t0; t <= t1; t += dx ) {
        if (iso_value < this->Gamma(this->u(ray.eval(t)))) {
            return t;
        }
    }

	return -1.0f;
}

Vector3 Cell::u( const Vector3 & p ) const
{
	Vector3 uvw = ( p - A() ) / ( G() - A() ); // gives the reference coordinates of the world space point p inside this cell (uvw is in the range <0,1>^3)

	return uvw;
}

Vector3 Cell::A() const
{
	return bounds_.lower_bound();
}

Vector3 Cell::G() const
{
	return bounds_.upper_bound();
}

float Cell::rho_A() const
{
	return rhos_[0];
}

float Cell::rho_B() const
{
	return rhos_[1];
}

float Cell::rho_C() const
{
	return rhos_[2];
}

float Cell::rho_D() const
{
	return rhos_[3];
}

float Cell::rho_E() const
{
	return rhos_[4];
}

float Cell::rho_F() const
{
	return rhos_[5];
}

float Cell::rho_G() const
{
	return rhos_[6];
}

float Cell::rho_H() const
{
	return rhos_[7];
}

Vector3 eye = Vector3( 500.0f, 400.0f, 1300.0f );
Vector3 light = Vector3( 500.0f, 500.0f, 500.0f );

//  𝑟=𝑑−2(𝑑⋅𝑛)𝑛
Vector3 reflect(Vector3 n, Vector3 d) {
    return d - 2 * d.DotProduct(n) * n;
}

float max (float a, float b) {
    return a > b ? a : b;
}
void Cell::DrawOnlyBones(Ray ray, float t0, float t1, Vector3 &out_color, float &out_alpha) {
    const float t_hit = FindIsoSurface( ray, t0, t1, 0.5f );
    if (t_hit > 0.0f) {
        const Vector3 hit_pos = ray.eval( t_hit );

        // TODO FOR POINT p: COMPUTE GRADIENT -> NORMAL -> LAMBERT SHADING + OMNI LIGHT -> SURFACE COLOR

        Vector3 normal = GradGamma(hit_pos);
        Vector3 hitToLight = light - hit_pos;
        hitToLight.Normalize();
        float diffuse = hitToLight.DotProduct(normal);


        float color = diffuse;
        out_color = Vector3(color, color, color);
        out_alpha = 1.0f;
    }

}


float transfer_function(float x, float px[4], float py[2]) {
    if (x <= px[0] || x >= px[3]) return py[0];
    if (x >= px[1] && x <= px[2]) return py[1];
    if (x < px[1])
        return py[0] + (x-px[0])*(px[1]-px[0]) * (py[1]-py[0]);
    return py[1] - (x-px[2])*(px[3]-px[2]) * (py[1]-py[0]);
}

Vector3 transfer_function_color(float x) {
    float px_r[4] = {0.0f, 0.2f, 0.3f, 0.5f};
    float px_g[4] = {0.2f, 0.5f, 0.7f, 0.9f};
    float px_b[4] = {0.7f, 0.9f, 1.0f, 1.5f};
    float py[2] = {0.0f, 1.0f};

    return Vector3 {
            transfer_function(x, px_r, py),
            transfer_function(x, px_g, py),
            transfer_function(x, px_b, py)
    };
}

void Cell::FrontToBack(Ray ray, float t0, float t1, Vector3& out_color, float& out_alpha) {

    float px_alpha[4] = {0.3f, 0.8f, 0.9f, 1.3f};
    float py_alpha[2] = {0.0f, 0.6f};

    float dx = 0.3f;
    for ( float t = t0; t <= t1; t += dx ) {

        const Vector3 hit_pos = ray.eval( t );
        auto uvw = this->u(hit_pos);
        float sample = this->Gamma(uvw);

        float in_alpha = out_alpha;
        Vector3 in_Color = out_color;
        float alpha = transfer_function(sample, px_alpha, py_alpha);

        out_alpha = in_alpha + (1-in_alpha) * alpha;

        Vector3 n = this->GradGamma(hit_pos);
        Vector3 L = light - hit_pos;
        L.Normalize();
        auto LN = L.DotProduct(n);
        Vector3 color = Vector3(LN, LN, LN);

        out_color = in_Color + (1 - in_alpha) * alpha * color * transfer_function_color(sample);


    }

}
