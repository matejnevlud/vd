import matplotlib.pyplot as plt
import numpy as np


np.random.seed(10)
collectn_1 = np.random.normal(100, 10, 200)
data = np.random.uniform(10, 100, 200)

data = [ 12.5, 13.4, 12.8, 12.9, 13.8, 14.2, 12.8, 13.8, 12.8, 16.7, 15.7, 17.4, 13.4, 13.3, 11.2, 12.7, 12.4, 15.4, 15.1, 14.2, 13.1, 13.3, 13.4, 13.8, 13.3, 13.9, 13.3, 14.1, 15.3, 13.4, 14.5 ]

quartile1, medians, quartile3 = np.percentile(data, [25, 50, 75])
print("Quartile 1: " + str(quartile1))
print("Median	 : " + str(medians))
print("Quartile 3: " + str(quartile3))


fig = plt.figure()

# Draw violin
violin = plt.violinplot(data, showmeans=False, showmedians=False, showextrema=False)
plt.title('Default plt plot')
plt.xlabel('Měsíc')
plt.ylabel('Teplota [°C]')

# Set plot colors
violin['bodies'][0].set_facecolor('#fff')
violin['bodies'][0].set_edgecolor('black')
violin['bodies'][0].set_alpha(1)

# Draw quartile line
plt.vlines(1, quartile1, quartile3, color='k', linestyle='-', capstyle='round', lw=5)
plt.scatter(1, medians, zorder=1000, color='w', s=20)

x = np.array([5,7,8,7,2,17,2,9,4,11,12,9,6])
y = np.array([99,86,87,88,111,86,103,87,94,78,77,85,86])

plt.scatter(np.ones(len(data)), data)
plt.show()