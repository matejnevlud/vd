import numpy as np
import matplotlib.pyplot as plt

def sum_of_order(data, order):
	sum = 0
	for i in range(len(data)):
		sum += pow(data[i], order)
	return sum

def sum_of_order_times(values, points, order):
	sum = 0
	for i in range(len(points)):
		sum += pow(points[i], order) * values[i]
	return sum


values =[
  -7.0,
  -2.2,
  -1.0,
    .1,
   2.2,
   3.2,
   3.5,
   4.1,
   4.0,
   2.0,
   3.7,
   3.7,
   2.7,
   2.9,
   2.2,
    .1,
   1.2,
   .3,
    .5,
   .5,
   .3,
  -1.3,
  -5.0,
 -10.4,
 -11.1,
  -8.3,
]

points = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26]

def printMatrix(A):
    n = len(A)
    for i in range(0, n):
        line = ""
        for j in range(0, n +1):
            line += str(A[i][j]) + "\t"
            if j == n - 1:
                line += "| "
        print(line)
    print("")


def gauss(A):
    n = len(A)

    for i in range(0, n):
        # Search for maximum in this column
        maxEl = abs(A[i][i])
        maxRow = i
        for k in range(i + 1, n):
            if abs(A[k][i]) > maxEl:
                maxEl = abs(A[k][i])
                maxRow = k

        # Swap maximum row with current row (column by column)
        for k in range(i, n + 1):
            tmp = A[maxRow][k]
            A[maxRow][k] = A[i][k]
            A[i][k] = tmp

        # Make all rows below this one 0 in current column
        for k in range(i + 1, n):
            c = -A[k][i] / A[i][i]
            for j in range(i, n + 1):
                if i == j:
                    A[k][j] = 0
                else:
                    A[k][j] += c * A[i][j]

    # Solve equation Ax=b for an upper triangular matrix A
    x = [0 for i in range(n)]
    for i in range(n - 1, -1, -1):
        x[i] = A[i][n] / A[i][i]
        for k in range(i - 1, -1, -1):
            A[k][n] -= A[k][i] * x[i]
    return x

def main():

	# Print points
	plt.scatter(points, values)

	n = len(values)

	sum_x = sum(points)
	sum_x2 = sum_of_order(points, 2)
	sum_x3 = sum_of_order(points, 3)
	sum_x4 = sum_of_order(points, 4)

	sum_y = sum(values)
	sum_xy = sum_of_order_times(values, points, 1)
	sum_x2y = sum_of_order_times(values, points, 2)

	# Assemble upper triangular matrix 
	matrix = [
		[n, 		sum_x,		sum_x2, 	sum_y],
		[sum_x,		sum_x2,		sum_x3, 	sum_xy],
		[sum_x2,	sum_x3, 	sum_x4, 	sum_x2y]
	]

	# Print the matrix
	printMatrix(matrix)

	# Run Gaussian elimination
	eval = gauss(matrix)
	
	
	print(eval)
	# create 1000 equally spaced points between -10 and 10
	plot_x = np.linspace(1, 26, 100)
	plot_y = np.zeros(100)
	for i in range(len(plot_x)):
		plot_y[i] = eval[0] + plot_x[i]*eval[1] + plot_x[i]*plot_x[i]*eval[2]

	plt.plot(plot_x, plot_y)
	


	# Reference
	#polynomial fit with degree = 2
	model = np.poly1d(np.polyfit(points, values, 2))

	#add fitted polynomial line to scatterplot
	polyline = np.linspace(1, 25, 50)
	plt.plot(polyline, model(polyline))


	plt.show()

main()