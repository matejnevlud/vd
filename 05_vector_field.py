from random import random
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as ani
from numpy.random.mtrand import multivariate_normal
from sklearn.neighbors import NearestNeighbors
import cv2

CMAP = 'hot'


class Point:
	x = 0.0
	y = 0.0
	history = []
	streak = False

	def __init__(self, x_, y_, streak_ = False):
		self.x = float(x_)
		self.y = float(y_)
		self.streak = streak_

	def __add__(self, p: 'Point'):
		return Point(self.x + p.x, self.y + p.y)
	
	def __sub__(self, p: 'Point'):
		return Point(self.x - p.x, self.y - p.y)

	def __mul__(self, c: float):
		return Point(self.x * c, self.y * c)


	def move(self, n_x_y):
		self.history.append((self.x, self.y))
		self.x = self.x + n_x_y[0]
		self.y = self.y + n_x_y[1]

		if(self.x < 0.0): self.x = 0.0
		if(self.x > 255.0): self.x = 255.0

		if(self.y < 0.0): self.y = 0.0
		if(self.y > 255.0): self.y = 255.0

	def setNextPosition(self, n_x_y):
		self.history.append((self.x, self.y))
		self.x = n_x_y[0]
		self.y = n_x_y[1]


		if(self.x < 0.0): self.x = 0.0
		if(self.x > 255.0): self.x = 255.0

		if(self.y < 0.0): self.y = 0.0
		if(self.y > 255.0): self.y = 255.0

	def rounded(self):
		x_t, y_t = np.int16(np.around((self.x, self.y)))
		return  y_t, x_t

	def __str__(self):
		return "(" + str(self.x) + ", " + str(self.y) + ")"

	def to_array(self): 
		return [self.x, self.y]

DEMO_POINT = Point(54.0, 180.0)

def random256():	
	return random() * 250.0

def dotProduct(a, b):
	return a[0] * b[0] + a[1] * b[1]

def mag(a):
	return np.sqrt(pow(a[0], 2) + pow(a[1], 2))

def odeEuler(f,y0,t):
    '''Approximate the solution of y'=f(y,t) by Euler's method.

    Parameters
    ----------
    f : function
        Right-hand side of the differential equation y'=f(t,y), y(t_0)=y_0
    y0 : number
        Initial value y(t0)=y0 wher t0 is the entry at index 0 in the array t
    t : array
        1D NumPy array of t values where we approximate y values. Time step
        at each iteration is given by t[n+1] - t[n].

    Returns
    -------
    y : 1D NumPy array
        Approximation y[n] of the solution y(t_n) computed by Euler's method.
    '''
    y = np.zeros(len(t))
    y[0] = y0
    for n in range(0,len(t)-1):
        y[n+1] = y[n] + f(y[n],t[n])*(t[n+1] - t[n])
    return y


# A sample differential equation "dy / dx = (x - y)/2"
def dydx(x, y):
    return ((x - y)/2)
 
# Finds value of y for a given x using step size h
# and initial value y0 at x0.
def rungeKutta(x0, y0, x, h):
    # Count number of iterations using step size or
    # step height h
    n = (int)((x - x0)/h)
    # Iterate for number of iterations
    y = y0
    for i in range(1, n + 1):
        "Apply Runge Kutta Formulas to find next value of y"
        k1 = h * dydx(x0, y)
        k2 = h * dydx(x0 + 0.5 * h, y + 0.5 * k1)
        k3 = h * dydx(x0 + 0.5 * h, y + 0.5 * k2)
        k4 = h * dydx(x0 + h, y + k3)
 
        # Update next value of y
        y = y + (1.0 / 6.0)*(k1 + 2 * k2 + 2 * k3 + k4)
 
        # Update next value of x
        x0 = x0 + h
    return y


def generateRandomPoints(num = 10):
	if num == 1: return [DEMO_POINT]
	randomPoints = []
	for i in range(num):
		randomPoints.append(Point(random256(), random256()))
	return randomPoints


randomPoints = generateRandomPoints(100)
STRENGTH = 2.0


def rot(vector_1d, x, y):


	mk = 254
	vc = ((x-1)*mk) + (y*2)
	vd = ((x+1)*mk) + (y*2)
	va = ((x)*mk) + ((y-1)*2)
	vb = ((x)*mk) + ((y+1)*2)

	frac1 = (vector_1d[vb+1] - vector_1d[va+1])/2
	frac2 = (vector_1d[vd] - vector_1d[vc])/2

	return (frac1-frac2)[0]


def main():
	plt.ion()
	for time in range(000,1000,1):
		print("Current timeframe " + "{:05d}".format(time))
		# Load current vector flow map
		file_open = cv2.FileStorage("./flow_field/u" + "{:05d}".format(time) + ".yml", cv2.FILE_STORAGE_READ)
		file_node: int = file_open.getNode("flow")
		vector_mat = file_node.mat()
		

		# Draw to raster
		testRaster = []
		for y in range(256):
			testRaster.append([])
			for x in range(256):
				testRaster[y].append(mag(vector_mat[y, x]))

		# Plot raster
		plt.figure(3)
		plt.imshow(testRaster, cmap=CMAP, interpolation='nearest')
		plt.colorbar()
		# Cycle through every point and move it
		for point in randomPoints:
			# Find closest vector to point
			x_point, y_point = np.int16(np.around((point.x, point.y)))
			closest_vector = vector_mat[y_point, x_point]  * STRENGTH
			point.move(closest_vector)


		
		# Cycle through every point and draw it
		for point in randomPoints:
			if not point.streak: 
				plt.scatter(point.x, point.y, zorder=10000000, c="blue", s=12)


		# Filter only streak points
		streakPoints = []
		for point in randomPoints: 
			if point.streak: 
				streakPoints.append(point)

		# Draw streak line
		for i in range(len(streakPoints) - 1):
			p = streakPoints[i]
			p_next = streakPoints[i + 1]
			plt.plot([p.x, p_next.x], [p.y, p_next.y], zorder=100000000, c="green")


		# Add next point to same spot
		randomPoints.append(Point(43.0, 240.0, True))

		plt.savefig("./flow_field_out/" + "{:05d}".format(time) + ".jpg")
		plt.draw()
		plt.pause(0.00001)
		plt.clf()

def mainRK4():
	plt.ion()
	for time in range(000,1000,1):
		print("Current timeframe " + "{:05d}".format(time))
		# Load current vector flow map
		file_open = cv2.FileStorage("./flow_field/u" + "{:05d}".format(time) + ".yml", cv2.FILE_STORAGE_READ)
		file_node: int = file_open.getNode("flow")
		vector_mat = file_node.mat()

		vector_1d = []
		for i in range(256):
			for j in range(256):
				vector_1d.append(vector_mat[j, i])

		# Draw to raster
		testRaster = []
		for y in range(256):
			testRaster.append([])
			for x in range(256):
				#testRaster[y].append(mag(vector_mat[y, x]))
				testRaster[y].append(rot(vector_1d, x, y))

		# Plot raster
		plt.figure(3)
		plt.imshow(testRaster, cmap=CMAP, interpolation='nearest')
		plt.colorbar()
		# Cycle through every point and move it
		for x_t in randomPoints:
			# Find closest vector to point
			K1 = vector_mat[x_t.rounded()]

			K2_p_y = x_t.y + (K1[0] * 0.5)
			K2_p_x = x_t.x + (K1[1] * 0.5)
			K2_x, K2_y = np.int16(np.around((K2_p_x, K2_p_y)))
			K2 = vector_mat[K2_y, K2_x]

			K3_p_y = x_t.y + (K2[0] * 0.5)
			K3_p_x = x_t.x + (K2[1] * 0.5)
			K3_x, K3_y = np.int16(np.around((K3_p_x, K3_p_y)))
			K3 = vector_mat[K3_y, K3_x]

			K4_p_y = x_t.y + K3[0]
			K4_p_x = x_t.x + K3[1]
			K4_x, K4_y = np.int16(np.around((K4_p_x, K4_p_y)))
			K4 = vector_mat[K4_y, K4_x]

			next_pos = ((K1[0] + K2[0] + K3[0] + K4[0]) * STRENGTH / 6.0, (K1[1] + K2[1] + K3[1] + K4[1]) * STRENGTH / 6.0)
			
			x_t.move(next_pos)


		
		# Cycle through every point and draw it
		for point in randomPoints:
			if not point.streak: 
				plt.scatter(point.x, point.y, zorder=10000000, c="blue", s=12)


		# Filter only streak points
		streakPoints = []
		for point in randomPoints: 
			if point.streak: 
				streakPoints.append(point)

		# Draw streak line
		for i in range(len(streakPoints) - 1):
			p = streakPoints[i]
			p_next = streakPoints[i + 1]
			plt.plot([p.x, p_next.x], [p.y, p_next.y], zorder=100000000, c="green")


		# Add next point to same spot
		randomPoints.append(Point(43.0, 240.0, True))

		plt.savefig("./flow_field_out/" + "{:05d}".format(time) + "_rk2.jpg")
		plt.draw()
		plt.pause(0.00001)
		plt.clf()


mainRK4()

void Cell::DrawOnlyBones(Ray ray, float t0, float t1, Vector3 &out_color, float &out_alpha) {
    const float t_hit = FindIsoSurface( ray, t0, t1, 0.5f );
    if (t_hit > 0.0f) {
        const Vector3 hit_pos = ray.eval( t_hit );

        // TODO FOR POINT p: COMPUTE GRADIENT -> NORMAL -> LAMBERT SHADING + OMNI LIGHT -> SURFACE COLOR

        Vector3 normal = GradGamma(hit_pos);
        Vector3 hitToLight = light - hit_pos;
        hitToLight.Normalize();
        float diffuse = hitToLight.DotProduct(normal);

        Vector3 eyeToHit = eye - hit_pos;
        eyeToHit.Normalize();
        Vector3 reflectDir = reflect(normal, -hitToLight);
        float specular = pow(max(eyeToHit.DotProduct(reflectDir), 0.0), 8);


        float color = diffuse * 0.9 + specular ;
        out_color = Vector3(color, color, color);
        out_alpha = 1.0f;
    }

}