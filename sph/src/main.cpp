// Include standard headers
#include <stdio.h>
#include <stdlib.h>

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>
GLFWwindow* window;

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
using namespace glm;

#include "shader.hpp"
#include <fstream>
#include <iostream>
#include <vector>
#include "polygonise.hpp"
#include "octree.hpp"
#include "half.hpp"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_writer.hpp"
#include <chrono>


using half_float::half;

using namespace std;

#pragma pack(1)
struct Point3h {
	half x;
	half y;
	half z;

	Point3h(){};

	Point3h(int _x, int _y, int _z) {
		x = (half) _x;
		y = (half) _y;
		z = (half) _z;
	};
	Point3h(float _x, float _y, float _z) {
		x = (half) _x;
		y = (half) _y;
		z = (half) _z;
	};
};

#pragma pack(1)
struct ParticleH {
	Point3h position; // particle position (m)

	Point3h velocity; // particle velocity (m/s)

	half rho; // density (kg/m3)
	half pressure;
	half radius; // particle radius (m)
};

class Point3f {
	public:
	Point3f(){}
  	Point3f(float x, float y, float z) : x(x), y(y), z(z){}

  	float x, y, z;
};

struct Particle {
	Point3f position; // particle position (m)
	float x;
	float y;
	float z;

	Point3f velocity; // particle velocity (m/s)

	float rho; // density (kg/m3)
	float pressure;
	float radius; // particle radius (m)


	Particle () {}

	Particle (ParticleH ph) {
		position = {(float)ph.position.x, (float)ph.position.y, (float)ph.position.z};
		velocity = {(float)ph.velocity.x, (float)ph.velocity.y, (float)ph.velocity.z};
		rho = (float)ph.rho;
		pressure = (float)ph.pressure;
		radius = (float)ph.radius;

		x = position.x;
		y = position.y;
		z = position.z;
	}
};

struct GridCell {
	Point3f position;
	vector<Particle> particles = {};
	float value = 0.0f;

	GridCell () {}
	GridCell (Point3f _p) : position(_p){}
};

struct Cube {
	vector<GridCell> verticies = {};
	vector<Particle> particles = {};
};


vector<GLfloat> demoCube(vector<Cube> cubes) {
	vector<GLfloat> g_vertex_buffer_data = {};

	for (int i = 0; i < cubes.size(); i+= 1) {
		Cube c = cubes[i];

	
		g_vertex_buffer_data.push_back(c.verticies[0].position.x);
		g_vertex_buffer_data.push_back(c.verticies[0].position.y);
		g_vertex_buffer_data.push_back(c.verticies[0].position.z);
		g_vertex_buffer_data.push_back(c.verticies[1].position.x);
		g_vertex_buffer_data.push_back(c.verticies[1].position.y);
		g_vertex_buffer_data.push_back(c.verticies[1].position.z);
		g_vertex_buffer_data.push_back(c.verticies[2].position.x);
		g_vertex_buffer_data.push_back(c.verticies[2].position.y);
		g_vertex_buffer_data.push_back(c.verticies[2].position.z);
		
		g_vertex_buffer_data.push_back(c.verticies[2].position.x);
		g_vertex_buffer_data.push_back(c.verticies[2].position.y);
		g_vertex_buffer_data.push_back(c.verticies[2].position.z);
		g_vertex_buffer_data.push_back(c.verticies[3].position.x);
		g_vertex_buffer_data.push_back(c.verticies[3].position.y);
		g_vertex_buffer_data.push_back(c.verticies[3].position.z);
		g_vertex_buffer_data.push_back(c.verticies[0].position.x);
		g_vertex_buffer_data.push_back(c.verticies[0].position.y);
		g_vertex_buffer_data.push_back(c.verticies[0].position.z);



		g_vertex_buffer_data.push_back(c.verticies[4].position.x);
		g_vertex_buffer_data.push_back(c.verticies[4].position.y);
		g_vertex_buffer_data.push_back(c.verticies[4].position.z);
		g_vertex_buffer_data.push_back(c.verticies[5].position.x);
		g_vertex_buffer_data.push_back(c.verticies[5].position.y);
		g_vertex_buffer_data.push_back(c.verticies[5].position.z);
		g_vertex_buffer_data.push_back(c.verticies[6].position.x);
		g_vertex_buffer_data.push_back(c.verticies[6].position.y);
		g_vertex_buffer_data.push_back(c.verticies[6].position.z);
		
		g_vertex_buffer_data.push_back(c.verticies[6].position.x);
		g_vertex_buffer_data.push_back(c.verticies[6].position.y);
		g_vertex_buffer_data.push_back(c.verticies[6].position.z);
		g_vertex_buffer_data.push_back(c.verticies[7].position.x);
		g_vertex_buffer_data.push_back(c.verticies[7].position.y);
		g_vertex_buffer_data.push_back(c.verticies[7].position.z);
		g_vertex_buffer_data.push_back(c.verticies[4].position.x);
		g_vertex_buffer_data.push_back(c.verticies[4].position.y);
		g_vertex_buffer_data.push_back(c.verticies[4].position.z);



		g_vertex_buffer_data.push_back(c.verticies[4].position.x);
		g_vertex_buffer_data.push_back(c.verticies[4].position.y);
		g_vertex_buffer_data.push_back(c.verticies[4].position.z);
		g_vertex_buffer_data.push_back(c.verticies[5].position.x);
		g_vertex_buffer_data.push_back(c.verticies[5].position.y);
		g_vertex_buffer_data.push_back(c.verticies[5].position.z);
		g_vertex_buffer_data.push_back(c.verticies[1].position.x);
		g_vertex_buffer_data.push_back(c.verticies[1].position.y);
		g_vertex_buffer_data.push_back(c.verticies[1].position.z);
		
		g_vertex_buffer_data.push_back(c.verticies[1].position.x);
		g_vertex_buffer_data.push_back(c.verticies[1].position.y);
		g_vertex_buffer_data.push_back(c.verticies[1].position.z);
		g_vertex_buffer_data.push_back(c.verticies[0].position.x);
		g_vertex_buffer_data.push_back(c.verticies[0].position.y);
		g_vertex_buffer_data.push_back(c.verticies[0].position.z);
		g_vertex_buffer_data.push_back(c.verticies[4].position.x);
		g_vertex_buffer_data.push_back(c.verticies[4].position.y);
		g_vertex_buffer_data.push_back(c.verticies[4].position.z);



		g_vertex_buffer_data.push_back(c.verticies[7].position.x);
		g_vertex_buffer_data.push_back(c.verticies[7].position.y);
		g_vertex_buffer_data.push_back(c.verticies[7].position.z);
		g_vertex_buffer_data.push_back(c.verticies[6].position.x);
		g_vertex_buffer_data.push_back(c.verticies[6].position.y);
		g_vertex_buffer_data.push_back(c.verticies[6].position.z);
		g_vertex_buffer_data.push_back(c.verticies[2].position.x);
		g_vertex_buffer_data.push_back(c.verticies[2].position.y);
		g_vertex_buffer_data.push_back(c.verticies[2].position.z);
		
		g_vertex_buffer_data.push_back(c.verticies[2].position.x);
		g_vertex_buffer_data.push_back(c.verticies[2].position.y);
		g_vertex_buffer_data.push_back(c.verticies[2].position.z);
		g_vertex_buffer_data.push_back(c.verticies[3].position.x);
		g_vertex_buffer_data.push_back(c.verticies[3].position.y);
		g_vertex_buffer_data.push_back(c.verticies[3].position.z);
		g_vertex_buffer_data.push_back(c.verticies[7].position.x);
		g_vertex_buffer_data.push_back(c.verticies[7].position.y);
		g_vertex_buffer_data.push_back(c.verticies[7].position.z);



		g_vertex_buffer_data.push_back(c.verticies[5].position.x);
		g_vertex_buffer_data.push_back(c.verticies[5].position.y);
		g_vertex_buffer_data.push_back(c.verticies[5].position.z);
		g_vertex_buffer_data.push_back(c.verticies[6].position.x);
		g_vertex_buffer_data.push_back(c.verticies[6].position.y);
		g_vertex_buffer_data.push_back(c.verticies[6].position.z);
		g_vertex_buffer_data.push_back(c.verticies[2].position.x);
		g_vertex_buffer_data.push_back(c.verticies[2].position.y);
		g_vertex_buffer_data.push_back(c.verticies[2].position.z);
		
		g_vertex_buffer_data.push_back(c.verticies[2].position.x);
		g_vertex_buffer_data.push_back(c.verticies[2].position.y);
		g_vertex_buffer_data.push_back(c.verticies[2].position.z);
		g_vertex_buffer_data.push_back(c.verticies[1].position.x);
		g_vertex_buffer_data.push_back(c.verticies[1].position.y);
		g_vertex_buffer_data.push_back(c.verticies[1].position.z);
		g_vertex_buffer_data.push_back(c.verticies[5].position.x);
		g_vertex_buffer_data.push_back(c.verticies[5].position.y);
		g_vertex_buffer_data.push_back(c.verticies[5].position.z);



		g_vertex_buffer_data.push_back(c.verticies[4].position.x);
		g_vertex_buffer_data.push_back(c.verticies[4].position.y);
		g_vertex_buffer_data.push_back(c.verticies[4].position.z);
		g_vertex_buffer_data.push_back(c.verticies[7].position.x);
		g_vertex_buffer_data.push_back(c.verticies[7].position.y);
		g_vertex_buffer_data.push_back(c.verticies[7].position.z);
		g_vertex_buffer_data.push_back(c.verticies[3].position.x);
		g_vertex_buffer_data.push_back(c.verticies[3].position.y);
		g_vertex_buffer_data.push_back(c.verticies[3].position.z);
		
		g_vertex_buffer_data.push_back(c.verticies[3].position.x);
		g_vertex_buffer_data.push_back(c.verticies[3].position.y);
		g_vertex_buffer_data.push_back(c.verticies[3].position.z);
		g_vertex_buffer_data.push_back(c.verticies[0].position.x);
		g_vertex_buffer_data.push_back(c.verticies[0].position.y);
		g_vertex_buffer_data.push_back(c.verticies[0].position.z);
		g_vertex_buffer_data.push_back(c.verticies[4].position.x);
		g_vertex_buffer_data.push_back(c.verticies[4].position.y);
		g_vertex_buffer_data.push_back(c.verticies[4].position.z);

	}

return g_vertex_buffer_data;
}






/*
   Linearly interpolate the position where an isosurface cuts
   an edge between two vertices, each with their own scalar value
*/
Point3f VertexInterp(float isolevel, Point3f p1, Point3f p2, float valp1 = 1.0f, float valp2 = 0.0f)
{
   float mu;
   Point3f p;

   if (abs(isolevel - valp1) < 0.00001)
      return(p1);
   if (abs(isolevel - valp2) < 0.00001)
      return(p2);
   if (abs(valp1 - valp2) < 0.00001)
      return(p1);
   mu = (isolevel - valp1) / (valp2 - valp1);
   p.x = p1.x + mu * (p2.x - p1.x);
   p.y = p1.y + mu * (p2.y - p1.y);
   p.z = p1.z + mu * (p2.z - p1.z);

   return(p);
}

vector<GLfloat> marchingCubes(vector<Cube> cubes) {
	vector<GLfloat> g_vertex_buffer_data = {};

	const float isolevel = 800.0f;

	for (int i = 0; i < cubes.size(); i+= 1) {
		Cube c = cubes[i];
		uint8_t cubeindex = 0;

		if (c.verticies[0].value < isolevel) cubeindex |= 1;
		if (c.verticies[1].value < isolevel) cubeindex |= 2;
		if (c.verticies[2].value < isolevel) cubeindex |= 4;
		if (c.verticies[3].value < isolevel) cubeindex |= 8;
		if (c.verticies[4].value < isolevel) cubeindex |= 16;
		if (c.verticies[5].value < isolevel) cubeindex |= 32;
		if (c.verticies[6].value < isolevel) cubeindex |= 64;
		if (c.verticies[7].value < isolevel) cubeindex |= 128;

		/* Cube is entirely in/out of the surface */
		if (edgeTable[cubeindex] == 0) continue;

		
		/* Find the vertices where the surface intersects the cube */
		Point3f vertlist[12];
		if (edgeTable[cubeindex] & 1)
			vertlist[0] = VertexInterp(isolevel,c.verticies[0].position,c.verticies[1].position,c.verticies[0].value,c.verticies[1].value);
		if (edgeTable[cubeindex] & 2)
			vertlist[1] = VertexInterp(isolevel,c.verticies[1].position,c.verticies[2].position,c.verticies[1].value,c.verticies[2].value);
		if (edgeTable[cubeindex] & 4)
			vertlist[2] = VertexInterp(isolevel,c.verticies[2].position,c.verticies[3].position,c.verticies[2].value,c.verticies[3].value);
		if (edgeTable[cubeindex] & 8)
			vertlist[3] = VertexInterp(isolevel,c.verticies[3].position,c.verticies[0].position,c.verticies[3].value,c.verticies[0].value);
		if (edgeTable[cubeindex] & 16)
			vertlist[4] = VertexInterp(isolevel,c.verticies[4].position,c.verticies[5].position,c.verticies[4].value,c.verticies[5].value);
		if (edgeTable[cubeindex] & 32)
			vertlist[5] = VertexInterp(isolevel,c.verticies[5].position,c.verticies[6].position,c.verticies[5].value,c.verticies[6].value);
		if (edgeTable[cubeindex] & 64)
			vertlist[6] = VertexInterp(isolevel,c.verticies[6].position,c.verticies[7].position,c.verticies[6].value,c.verticies[7].value);
		if (edgeTable[cubeindex] & 128)
			vertlist[7] = VertexInterp(isolevel,c.verticies[7].position,c.verticies[4].position,c.verticies[7].value,c.verticies[4].value);
		if (edgeTable[cubeindex] & 256)
			vertlist[8] = VertexInterp(isolevel,c.verticies[0].position,c.verticies[4].position,c.verticies[0].value,c.verticies[4].value);
		if (edgeTable[cubeindex] & 512)
			vertlist[9] = VertexInterp(isolevel,c.verticies[1].position,c.verticies[5].position,c.verticies[1].value,c.verticies[5].value);
		if (edgeTable[cubeindex] & 1024)
			vertlist[10] = VertexInterp(isolevel,c.verticies[2].position,c.verticies[6].position,c.verticies[2].value,c.verticies[6].value);
		if (edgeTable[cubeindex] & 2048)
			vertlist[11] = VertexInterp(isolevel,c.verticies[3].position,c.verticies[7].position,c.verticies[3].value,c.verticies[7].value);

		/* Create the triangle */
		int no_tri = 0;
		for (int i = 0; triTable[cubeindex][i] != -1; i += 3) {
			Point3f t0 = vertlist[triTable[cubeindex][i  ]];
			Point3f t1 = vertlist[triTable[cubeindex][i+1]];
			Point3f t2 = vertlist[triTable[cubeindex][i+2]];

			g_vertex_buffer_data.push_back(t0.x);
			g_vertex_buffer_data.push_back(t0.y);
			g_vertex_buffer_data.push_back(t0.z);

			g_vertex_buffer_data.push_back(t1.x);
			g_vertex_buffer_data.push_back(t1.y);
			g_vertex_buffer_data.push_back(t1.z);

			g_vertex_buffer_data.push_back(t2.x);
			g_vertex_buffer_data.push_back(t2.y);
			g_vertex_buffer_data.push_back(t2.z);
			no_tri++;
		}

	}

	return g_vertex_buffer_data;
}



vector<Cube> generateCubeMatrix(vector<Particle> particles, unibn::Octree<Point3f> & searchTree, int SIZE_DOMAIN = 40) {

	const int NUM_OF_CUBES = SIZE_DOMAIN*SIZE_DOMAIN*SIZE_DOMAIN;
	const float S = 1.0f / (float)SIZE_DOMAIN;
	
	vector<Cube> cubes;

	int interationCount = (int)(1.0f / S);
	
	
	for (float y = -0.5 + S; y < 0.5; y += S) {
		for (float x = -0.5 + S; x < 0.5; x += S) {
			
			int no_non_empty_cubes = 0;
			for (float z = 1.0f; z > 0.0; z -= S) {
				Cube c;
				GridCell g0 = GridCell({x, y, z + S});
				GridCell g1 = GridCell({x + S, y, z + S});
				GridCell g2 = GridCell({x + S, y + S, z + S});
				GridCell g3 = GridCell({x, y + S, z + S});
				GridCell g4 = GridCell({x, y, z});
				GridCell g5 = GridCell({x + S, y, z});
				GridCell g6 = GridCell({x + S, y + S, z});
				GridCell g7 = GridCell({x, y + S, z});

				c.verticies.push_back(g0);
				c.verticies.push_back(g1);
				c.verticies.push_back(g2);
				c.verticies.push_back(g3);
				c.verticies.push_back(g4);
				c.verticies.push_back(g5);
				c.verticies.push_back(g6);
				c.verticies.push_back(g7);

				//! Closest in radius
				#pragma omp for
				for (int i = 0; i < c.verticies.size(); i++) {
					
					const Point3f& query = c.verticies[i].position;

					std::vector<uint32_t> results;
					searchTree.radiusNeighbors<unibn::L2Distance<Point3f>>(query, S * 1.5f, results);	
					
					//std::cout << i << " - " << results.size() << " radius neighbors (r = 0.01m) found for (" << query.x << ", " << query.y << "," << query.z << ")" << std::endl;

					for (uint32_t j = 0; j < results.size(); ++j) {
						const Particle& p = particles[results[j]];
						//std::cout << "  " << results[i] << ": (" << p.x << ", " << p.y << ", " << p.z << ") => " << std::sqrt(unibn::L2Distance<Point3f>::compute(p.position, query)) << std::endl;
						c.verticies[i].value += p.rho;
						
						c.particles.push_back(p);
					}
					c.verticies[i].value /= results.size();
					if (isnan(c.verticies[i].value)) c.verticies[i].value = 0;
					//printf("%f - Average rho: %f\n", z, c.verticies[i].value);
				}		
				
				cubes.push_back(c);

				if(!c.particles.empty()) no_non_empty_cubes++;
				if (no_non_empty_cubes > 1)	break;
					
				
			}
		}
	}

	return cubes;
}

void renderNextFrame(GLuint programID, GLuint MatrixID, glm::mat4 MVP, int frame) {

	const int GRID_SIZE = 10;

	//! Prepare video frame file path
	std::string s = std::to_string(frame % 5000);
	unsigned int number_of_zeros = 6 - s.length(); // add 2 zeros
	s.insert(0, number_of_zeros, '0');
	std::string pre = "./sph_17_40x40x40_mu_3_5/sph_";
	std::string type = ".bin";
	std::string path =  pre + s + type;
	

	std::ifstream fileStream;
	fileStream.open(path.c_str());

	//! Load particle data
	vector<Particle> particles;
	vector<Point3f> particle_positions;
	for (int i = 0; i < 40*40*40; ++i){
		ParticleH ph;
		fileStream.read((char*)&ph, sizeof(ParticleH));

		Particle p = Particle(ph);

		particles.push_back(p);
		particle_positions.push_back(p.position);
		//printf("Positions: %f, %f, %f : rho %f\n", p.position.x, p.position.y, p.position.z, p.rho);
	}

	//! Prepare kd tree search
	unibn::Octree<Point3f> searchTree;
	unibn::OctreeParams params;
	searchTree.initialize(particle_positions, params);

	//! Generate SPH cuve matrix
	vector<Cube> cubes = generateCubeMatrix(particles, searchTree, GRID_SIZE);




	//! Prepare marching cubes triangles
	vector<GLfloat> g_vertex_buffer_data = {};
	g_vertex_buffer_data = marchingCubes(cubes);
	const int triangleCount = g_vertex_buffer_data.size() / (3 * 3);

	//! Prepare vertex buffer
	GLuint vertexbuffer;
	glGenBuffers(1, &vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, g_vertex_buffer_data.size() * sizeof(GLfloat),  &g_vertex_buffer_data[0], GL_STATIC_DRAW);


	//! Generate normals
	vector<GLfloat> g_normal_buffer_data = {};
	for (int i = 0; i < g_vertex_buffer_data.size(); i+=9) {
		glm::vec3 t0 = glm::vec3(g_vertex_buffer_data[i], g_vertex_buffer_data[i + 1], g_vertex_buffer_data[i + 2]);
		glm::vec3 t1 = glm::vec3(g_vertex_buffer_data[i] + 3, g_vertex_buffer_data[i + 4], g_vertex_buffer_data[i + 5]);
		glm::vec3 t2 = glm::vec3(g_vertex_buffer_data[i] + 6, g_vertex_buffer_data[i + 7], g_vertex_buffer_data[i + 8]);

		glm::vec3 A = glm::vec3(t1.x - t0.x, t1.y - t0.y, t1.z - t0.z);
		glm::vec3 B = glm::vec3(t2.x - t0.x, t2.y - t0.y, t2.z - t0.z);

		glm::vec3 normal = glm::normalize(glm::cross(A, B));



		g_normal_buffer_data.push_back(normal.x);
		g_normal_buffer_data.push_back(normal.y);
		g_normal_buffer_data.push_back(normal.z);

		g_normal_buffer_data.push_back(normal.x);
		g_normal_buffer_data.push_back(normal.y);
		g_normal_buffer_data.push_back(normal.z);
		
		g_normal_buffer_data.push_back(normal.x);
		g_normal_buffer_data.push_back(normal.y);
		g_normal_buffer_data.push_back(normal.z);
	}

	//! Prepare normal buffer
	GLuint normalbuffer;
	glGenBuffers(1, &normalbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
	glBufferData(GL_ARRAY_BUFFER, g_normal_buffer_data.size() * sizeof(GLfloat),  &g_normal_buffer_data[0], GL_STATIC_DRAW);



	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(programID);
	
	glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);

	// 1rst attribute buffer : vertices
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

	// Draw the triangle !
	glDrawArrays(GL_TRIANGLES, 0, triangleCount * 3); // 12*3 indices starting at 0 -> 12 triangles
	//glDrawArrays(GL_LINE_STRIP, 0, triangleCount * 3); // 12*3 indices starting at 0 -> 12 triangles
	//glDrawArrays(GL_LINE_STRIP, 0, g_particles_buffer_data.size()/3); // 12*3 indices starting at 0 -> 12 triangles

	glDisableVertexAttribArray(0);

	// Swap buffers
	glfwSwapBuffers(window);
	glfwPollEvents();

}

void saveImage(int frame) {
	int width, height;
	glfwGetFramebufferSize(window, &width, &height);
	GLsizei nrChannels = 3;
	GLsizei stride = nrChannels * width;
	stride += (stride % 4) ? (4 - stride % 4) : 0;
	GLsizei bufferSize = stride * height;
	std::vector<char> buffer(bufferSize);
	glPixelStorei(GL_PACK_ALIGNMENT, 4);
	glReadBuffer(GL_FRONT);
	glReadPixels(0, 0, width, height, GL_RGB, GL_UNSIGNED_BYTE, buffer.data());

	std::string s = std::to_string(frame);
	unsigned int number_of_zeros = 6 - s.length(); // add 2 zeros
	s.insert(0, number_of_zeros, '0');
	std::string pre = "./render/";
	std::string type = ".png";
	std::string path =  pre + s + type;
	stbi_flip_vertically_on_write(true);
	stbi_write_png(path.c_str(), width, height, nrChannels, buffer.data(), stride);
}

int main( void )
{


	// Initialise GLFW
	if( !glfwInit() )
	{
		fprintf( stderr, "Failed to initialize GLFW\n" );
		getchar();
		return -1;
	}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef __APPLE__
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif
	// Open a window and create its OpenGL context
	window = glfwCreateWindow( 1024, 768, "Tutorial 04 - Colored Cube", NULL, NULL);
	if( window == NULL ){
		fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n" );
		getchar();
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	// Initialize GLEW
	glewExperimental = true; // Needed for core profile
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		getchar();
		glfwTerminate();
		return -1;
	}

	// Ensure we can capture the escape key being pressed below
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

	// Dark blue background
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	glPointSize(16);

	GLuint VertexArrayID;
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);

	// Create and compile our GLSL program from the shaders
	GLuint programID = LoadShaders( "TransformVertexShader.vertexshader", "ColorFragmentShader.fragmentshader" );
	GLuint MatrixID = glGetUniformLocation(programID, "MVP");
	glm::mat4 Projection = glm::perspective(glm::radians(60.0f), 4.0f / 3.0f, 0.1f, 100.0f);
	glm::mat4 View       = glm::lookAt(
								glm::vec3(1, 1, 1), // Camera is at (4,3,-3), in World Space
								glm::vec3(0,0,.5), // and looks at the origin
								glm::vec3(0,0,1)  // Head is up (set to 0,-1,0 to look upside-down)
						   );
	glm::mat4 Model      = glm::mat4(1.0f);
	glm::mat4 MVP        = Projection * View * Model;

	int deciFrame = 0;
	std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
	do{

		std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
		double delta = std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count();
		//if(delta < 1000) continue;
		std::cout << "Frame " << deciFrame << " - Time difference = " << delta << "[µs]" << std::endl;
		begin = std::chrono::steady_clock::now();


		renderNextFrame(programID, MatrixID, MVP, deciFrame);
		saveImage(deciFrame);

		deciFrame += 10;
	} // Check if the ESC key was pressed or the window was closed
	while( glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS && glfwWindowShouldClose(window) == 0  && deciFrame < 5000);

	// Cleanup VBO and shader
	//glDeleteBuffers(1, &vertexbuffer);
	glDeleteProgram(programID);
	glDeleteVertexArrays(1, &VertexArrayID);

	// Close OpenGL window and terminate GLFW
	glfwTerminate();

	return 0;
}
