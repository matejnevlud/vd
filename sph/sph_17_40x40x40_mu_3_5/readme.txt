BIN file format
---------------

Binary format containig array of particles with the following structure:

struct Particle
{
  half position_x; // particle position (m)
  half position_y;
  half position_z;

  half velocity_x; // particle velocity (m/s)
  half velocity_y;
  half velocity_z;

  half rho; // density (kg/m3)
  half pressure;
  half radius; // particle radius (m)
};

where half is 16-bit half-precision floating-point type from HALF 2.1.0 C++ header-only library [1].

Single particle mass m_i = 400 kg / 64000 = 0.00625 kg.

[1] http://half.sourceforge.net/index.html