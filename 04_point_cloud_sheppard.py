import numpy as np
import matplotlib.pyplot as plt
from numpy.random.mtrand import multivariate_normal
from sklearn.neighbors import NearestNeighbors
CMAP = 'hot'


def gaussian(x, mu, sig):
    return np.exp(-np.power(x - mu, 2.) / (2 * np.power(sig, 2.)))


# define normalized 2D gaussian
def gaus2d(x=0, y=0, mx=0, my=0, scale=100):
    return 1. / (2. * np.pi * scale * scale) * np.exp(-((x - mx)**2. / (2. * scale**2.) + (y - my)**2. / (2. * scale**2.)))


def generateXYPoints(numberOfPoints = 100):
	points = []
	for i in range(numberOfPoints):
		X,Y = np.random.normal(loc=0, scale=100, size=2)
		points.append([X, Y])

	return np.array(points)

def generateZPoints(point_cloud_XY):
	points = []
	for i in range(len(point_cloud_XY)):
		X,Y = point_cloud_XY[i]
		Z = gaus2d(X, Y)
		points.append(Z)

	return np.array(points)




point_cloud_XY = generateXYPoints(1000)
point_cloud_Z = generateZPoints(point_cloud_XY)
print(point_cloud_XY)
print(point_cloud_Z)



plt.figure(1)
sample_domain_plot = plt.axes(projection='3d')
sample_domain_plot.scatter3D(point_cloud_XY[:,0], point_cloud_XY[:,1], point_cloud_Z, c=point_cloud_Z, cmap=CMAP)

plt.figure(2)
plt.scatter(point_cloud_XY[:,0], point_cloud_XY[:,1], c=point_cloud_Z, cmap=CMAP)
plt.colorbar()


# Create nearest neighbors matrix
print("Building kNN distance matrix")
knn = NearestNeighbors(n_neighbors=5)
knn.fit(point_cloud_XY)


# Control parameters
LAMBDA = 0.001
MAX_DISTANCE = 75

raster = []
print("Rasterizing points")
y_id = 0
for y in range(-200, 200, 1):
	raster.append([])
	for x in range(-200, 200, 1):
		query_point = np.array([x, y])

		nearest_points_result = knn.kneighbors(query_point.reshape(1, -1), return_distance=True)
		nearest_points_idx = nearest_points_result[1][0]
		nearest_distances = nearest_points_result[0][0]
		
		R_p = nearest_distances[0];


		sum_numerator = 0.0
		sum_denominator = 0.0
		for i in range(len(nearest_points_idx)):
			if nearest_distances[i] > MAX_DISTANCE: break
			f_i = point_cloud_Z[nearest_points_idx[i]]

			sum_numerator += f_i * np.exp(LAMBDA * pow( abs(nearest_distances[i] / R_p), 2))
			sum_denominator += np.exp(LAMBDA * pow( abs(nearest_distances[i] / R_p), 2))

		f_lambda = sum_numerator / sum_denominator if sum_denominator != 0.0 else 0.0
		raster[y_id].append(f_lambda)
	y_id += 1
		
		

plt.figure(3)
plt.imshow(raster, cmap=CMAP, interpolation='nearest')
plt.colorbar()
plt.show()